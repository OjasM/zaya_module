from mongoengine import *

# Create your models here.

class User_Account(Document):
	student_id = IntField()
	Name = StringField(max_length=20)
	Age = IntField()
	Class = StringField(max_length=5)					# Class could be "3A" or "3B", hence StringField for Class
	
class Attendance(Document):
	attend_id = IntField()
	student_id = ReferenceField(User_Account)
	Date = DateTimeField()
	Status = IntField()									# Status should be 1 for present and 0 for absent
	
class Points(Document):
	points_id = IntField()
	student_id = ReferenceField(User_Account)
	points = IntField()
	
class Behaviour(Document):
	behaviour_id = IntField()
	Name = StringField(max_length=200)
	Points = IntField()