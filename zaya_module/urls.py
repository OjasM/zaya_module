from django.conf.urls import patterns, include, url

from views import *

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
	# home urls
    (r'^home/$', home_view),
	(r'^index/$', home_view),
	(r'^$', home_view),
	
	# user creation urls
	(r'new_user/$', create_user_view),
	
	# attendance urls
	(r'update_attendance/$', update_attendance_view),
	(r'submit_attendance/$', update_attendance_view),
	
	# behaviour points urls
	(r'behaviour_form/$',behaviour_form_view),
	(r'assign_points/$',behaviour_form_view),
	
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
)
