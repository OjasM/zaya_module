from zaya_module_db.models import *
from django.shortcuts import render
import re
import datetime

def home_view(request):
	return render(request, 'index.html',{'user_created':False}) 

def create_user_view(request):
	users_list = User_Account.objects.all()
	user = User_Account(student_id=len(users_list)+1,Name="User"+str(len(users_list)+1),Age=9,Class="3")
	user.save()
	return render(request, 'index.html',{'user_created':True,'user':user})

def update_attendance_view(request):
	users_list = User_Account.objects.all()
	attend_list = Attendance.objects.all()
	if request.POST.get('submit_attendance'):
                same_date=0
                date_entered=request.POST.get('todays_date', '')
                check_format=str(request.POST.get('todays_date', ''))

                searchObj = re.search('([0-9]{4}\-[0-9]{2}\-[0-9]{2})', check_format , re.M|re.I)
                if searchObj:
                        same_date=0																										
                else:
                        same_date=3																										# same_date=3 Date entered in incorrect format
								
                if not date_entered:
                        same_date=2																										# same_date=2 Date not entered
                for item in attend_list:
                    if str(item.Date.date())==str(request.POST.get('todays_date', '')):
                            same_date=1																									# same_date=1 Date already exists
                if same_date!=0:
                        return render(request, 'attendance_list.html' , {'users_list': users_list,'same_date':same_date} )
                else:
					i=1
					for user in users_list:
						attend_list = Attendance.objects.all()
						Attendance(attend_id=len(attend_list)+1,student_id=user,Date=request.POST.get('todays_date', ''),Status=int(request.POST.get("status"+str(i)))).save()
						i=i+1
					ADD_STATUS = 1
		return render(request, 'index.html', {'ADD_STATUS':ADD_STATUS,'i':i } )
	return render(request, 'attendance_list.html', {'users_list':users_list})

def behaviour_form_view(request):
	if request.method == 'POST':
		no_name=False
		no_behaviour_type=False
		user_name=request.POST.get('user_name','')
		if not user_name:
			no_name=True
		behaviour_type=request.POST.get('behaviour_type','')
		if not behaviour_type:
			no_behaviour_type=True
		
		if no_name or no_behaviour_type:
			return render(request, 'behaviour_form.html', {'no_name':no_name, 'no_behaviour_type':no_behaviour_type})
		else:
			points_list = Points.objects.all()
			user=User_Account.objects(Name__contains=user_name)
			#behaviour=Behaviour.objects(Name__contains=behaviour_type)
			#Points(points_id=len(points_list)+1,student_id=user,points=behaviour.Points).save()
			return render(request, 'behaviour_form.html', {'points_assigned':True,'user':user} )
	return render(request, 'behaviour_form.html',)
	
	
		